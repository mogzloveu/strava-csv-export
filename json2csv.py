import json
import csv
import datetime


def json_2_csv(data):
    data = json.loads(data)
    filename = datetime.datetime.now().strftime("%H%M%S %d-%m-%Y") + '_strava_export.csv'

    f = csv.writer(open(filename, "w"))

    f.writerow(["date", "time", "type", "name", "distance"])

    for param in data:
        date = datetime.datetime.strptime(param["start_date_local"], '%Y-%m-%dT%H:%M:%SZ').strftime('%m/%d/%Y')
        time = datetime.datetime.strptime(param["start_date_local"], '%Y-%m-%dT%H:%M:%SZ').strftime('%H:%M')

        f.writerow([date,
                    time,
                    param["type"],
                    param["name"],
                    str(param["distance"]/1000) + "km"])

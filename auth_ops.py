import json
import requests
import selenium
import os
import inspect
from urllib.parse import urlunsplit,urlencode,urlparse
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

client_id = 29274
redirect_uri = 'http://127.0.0.1'
with open('client_secret.txt') as file:
    client_secret = file.readline().strip()


def get_auth_code(scheme,server):
    code_params = {'client_id': client_id,
              'redirect_uri': redirect_uri,
              'approval_prompt': 'auto',
              'response_type': 'code',
              'scope': 'activity:read_all'}

    url = urlunsplit(('https', server, '/oauth/authorize', urlencode(code_params), ''))

    driver = webdriver.Chrome(os.path.join(
        os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        ,"chromedriver"))
    driver.get(url)
    element = WebDriverWait(driver, 60).until(EC.url_contains(('code=')))
    currentURL = driver.current_url
    code = urlparse(currentURL).query.replace('state=&code=', '').replace('&scope=read,activity:read_all', '')
    driver.quit()

    return code


def get_access_token(scheme,server,code):
    token_params = {'client_id': client_id,
              'client_secret': client_secret,
              'code': code}

    get_access_token_url = urlunsplit(('https', server, '/oauth/token', urlencode(token_params), ''))

    r = requests.post(get_access_token_url, data = token_params)
    access_token = json.loads(r.text)["access_token"]

    return access_token

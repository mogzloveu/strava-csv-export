import auth_ops
import strava_ops
import json2csv

scheme = 'https'
server = 'www.strava.com'

if __name__ == "__main__":
    # Getting authentication code
    code = auth_ops.get_auth_code(scheme, server)

    #Getting access token
    access_token = auth_ops.get_access_token(scheme, server, code)

    #Getting all the activities for this month
    data = strava_ops.get_all_activities_this_month(scheme, server, access_token)

    #Converting JSON data to CSV file
    json2csv.json_2_csv(data)

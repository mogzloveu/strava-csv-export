# import datetime
import arrow


def start_of_month():
    return arrow.utcnow().span('month')[0].format('X')


def end_of_month():
    return arrow.utcnow().span('month')[1].format('X')

import requests
import datetime_ops
from urllib.parse import urlunsplit,urlencode


def get_all_activities_this_month(scheme, server, access_token):
    before_date = datetime_ops.end_of_month()
    after_date = datetime_ops.start_of_month()
    activities_params = {'access_token': access_token,
              'before': before_date,
              'after': after_date,
              'per_page': 200}

    activities_url = urlunsplit((scheme, server, '/api/v3/athlete/activities', urlencode(activities_params), ''))

    data = requests.get(activities_url)

    return data.text

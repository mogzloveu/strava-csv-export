### strava-csv-export

A python script to export Strava activities into a CSV file.

### Pre-requites
1. Latest version of Python 3. This script was built on Python 3.7.0.
1. Need Chrome browser installed on the machine.

### How to use
1. Clone this repo.
1. Install all the required Python packages:  
	`cd <path to cloned folder>`  
	`pip3 install -r requirements.txt`
1. Download the ChromeDriver from [ChromeDriver Download](http://chromedriver.chromium.org/downloads) and place the driver in the cloned folder.
1. Place the `client_secret.txt` file after obtaining it from email.
1. Execute the script as per:  
	`python3 strava_csv_export.py`
1. The script will open a new Chrome window to prompt for Strava login.
1. Once your login is successful, the script will create a CSV file under the cloned folder.

### Note
1. This script will add my dummy Strava account under your [My Apps](https://www.strava.com/settings/apps) page. You can always revoke the access to the dummy account.
1. This script currently supports exporting current month activities only, however I will build more features in future depending on the demand.
1. The script needs the API client secret key from the dummy account, please email me via mmogavenasan@gmail.com and I will pass it to you.
